//-------------------------------------------------------------------------------------------------------------------------------------------------------------
// UMSATS 2018-2020
//
// Repository:
//  UMSATS > Avionics 2019
//
// File Description:
//  Altimeter readings interface
//
// History
// 2019-04-01 by Eric Kapilik
// - Created.
//-------------------------------------------------------------------------------------------------------------------------------------------------------------
#include "altimeter.h"
#include <math.h>
#include "configuration.h"

alt_value altitude_approx(float pressure, float temperature, configData_t *config)
{
	float p_term = pow((config->values.ref_pres / (pressure / 100)), (1 / 5.257F)) - 1;
	float t_term = (temperature / 100) + 273.15F;
	alt_value result;
	result.float_val = (p_term * t_term) / 0.0065F + config->values.ref_alt;
	return result;
	
}