// UMSATS 2018-2020
//
// Repository:
//  UMSATS/Avionics-2019
//
// File Description:
//  Source file for the data logging module.
//
// History
// 2019-04-10 by Joseph Howarth
// - Created.

#include "dataLogging.h"
#include "flash.h"
#include "buzzer.h"
#include "cmsis_os.h"				//For delay and queues
#include "recovery.h"
#include "altimeter.h"
#include "pressure_sensor_bmp3.h"	//For bmp reading struct
#include "sensorAG.h"				//For imu_reading struct

#include "utilities/common.h"

void _launch();
void _landing();
void _parachute_deployment();
void _ascent();
void _flash_write();
void _write_pressure_sensor_data(bmp_data_struct bmp_reading, uint8_t * dest);
void _write_imu_data(imu_data_struct src, uint8_t* dest);


//Checks if a measurement is empty. returns 0 if it is empty.
uint8_t is_measurement_container_empty(measurement_container_t *measurement)
{
	uint8_t result = 0;
	for(size_t i = 0; i < sizeof(measurement_container_t); i++)
	{
		if(measurement->data[i] != 0)
		{
			result++;
		}
	}
	return result;
}


void logging_task(void *params)
{
	LoggingStruct_t *logStruct = (LoggingStruct_t *) params;
	Flash *flash_ptr = logStruct->flash_handle;
	UART_HandleTypeDef *huart = logStruct->uart;
	configData_t *configParams = logStruct->flightCompConfig;
	TaskHandle_t *timerTask_h = logStruct->timerTask_h;
	uint32_t flash_address = FLASH_START_ADDRESS;
	
	if(IS_IN_FLIGHT(configParams->values.flags)){
		flash_address = configParams->values.end_data_address;
	}
	
	int32_t acc_z_filtered;
	uint8_t acc_z_filter_index = 0;
	float altFiltered = 0;
	uint8_t alt_filter_count = 0;
	uint8_t running = 1;
	uint8_t data_bufferA[FLASH_DATA_BUFFER_SIZE];        // This stores the data until we have enough to write to flash.
	uint8_t data_bufferB[FLASH_DATA_BUFFER_SIZE];        // This stores the data until we have enough to write to flash.
	uint16_t ring_buff_size = FLASH_DATA_BUFFER_SIZE * 25;
	uint8_t launchpadBuffer[ring_buff_size];
	BufferSelection_t buffer_selection = BUFFER_A;
	uint16_t buffer_current_pos = 0;                    // The current index in the buffer.
	
	measurement_container_t measurement_container;
	uint8_t measurement_length = 0;
	uint32_t prev_time_ticks = 0;    // Holds the previous time to calculate the change in time.
	
	uint8_t buf[120];
	
	HAL_GPIO_WritePin(USR_LED_PORT, USR_LED_PIN, GPIO_PIN_RESET);
	
	//Make sure the measurement starts empty.
	
	for(size_t i = 0; i < sizeof(measurement_container_t); i++)
	{
		measurement_container.data[i] = 0;
	}
	
	imu_data_struct imu_reading;
	bmp_data_struct bmp_reading;
	prev_time_ticks = xTaskGetTickCount();
	
	
	alt_value altitude;
	alt_value alt_prev;
	alt_prev.float_val = 0;
	uint8_t alt_count = 0;
	uint8_t alt_main_count = 0;
	uint16_t apogee_holdout_count = 0;
	uint8_t landed_counter = 0;
	
	if(!IS_IN_FLIGHT(configParams->values.flags))
	{
		recoverySelect_t event_d = DROGUE;
		continuityStatus_t cont_d = check_continuity(event_d);
		recoverySelect_t event_m = MAIN;
		continuityStatus_t cont_m = check_continuity(event_m);
		
		while(cont_m == OPEN_CIRCUIT || cont_d == OPEN_CIRCUIT)
		{
			
			cont_m = check_continuity(event_m);
			cont_d = check_continuity(event_d);
		}
		configParams->values.state = STATE_LAUNCHPAD_ARMED;
		write_config(configParams);
	}
	
	buzz(250); // CHANGE TO 2 SECONDS!!!!!!!
	while(1)
	{
		measurement_length = 0;
		/* IMU READING*******************************************************************************************************************************/
		//Try and get data from the IMU queue. Block for up to a quarter of the time between the fastest measurement.
		if(pdTRUE == xQueueReceive(logStruct->IMU_data_queue, &imu_reading, configParams->values.data_rate / 4))
		{
			if(!is_measurement_container_empty(&measurement_container)){
				_write_imu_data(imu_reading, &measurement_container.data[0]);
			}
			
			HAL_GPIO_TogglePin(USR_LED_PORT, USR_LED_PIN);
		}else
		{
			clear_buffer(measurement_container.data, sizeof(measurement_container_t));
			continue;
		}
		
		//Try and get data from the BMP queue. Block for up to a quarter of the time between the fastest measurement.
		if(pdTRUE == xQueueReceive(logStruct->PRES_data_queue, &bmp_reading, configParams->values.data_rate / 4))
		{
			if(is_measurement_container_empty(&measurement_container)){
				_write_pressure_sensor_data(bmp_reading, &measurement_container.data[0]);
			}
		}else
		{
			clear_buffer(measurement_container.data, sizeof(measurement_container_t));
			continue;
		}
		
		switch(configParams->values.state)
		{
			case STATE_XTRACT:
				/* empty */
			case STATE_LAUNCHPAD:
				/* empty */
				break;
			case STATE_LAUNCHPAD_ARMED:
				if(imu_reading.data_acc.x > 10892){
					_launch();
				}
				break;
			case STATE_IN_FLIGHT_PRE_APOGEE:
				_ascent();
				break;
			case STATE_IN_FLIGHT_POST_APOGEE:
				_parachute_deployment();
				break;
			case STATE_IN_FLIGHT_POST_MAIN:
				_landing();
				break;
			case STATE_LANDED:
				/* empty */
				break;
		}
		
		/* Fill Buffer and/or write to flash*********************************************************************************************************/
		if(is_measurement_container_empty(&measurement_container) &&
		   configParams->values.state == STATE_LAUNCHPAD_ARMED    &&
		   ((buffer_current_pos + measurement_length + HEADER_SIZE) <= ring_buff_size))
		{
			//check if room in launchpad buffer.
			memcpy(&launchpadBuffer[buffer_current_pos], &(measurement_container.data), measurement_length + HEADER_SIZE);
			
			buffer_current_pos += (measurement_length + HEADER_SIZE);
			buffer_current_pos = buffer_current_pos % (ring_buff_size);
			
			//Reset the measurement.
			clear_buffer(measurement_container.data, sizeof(measurement_container_t));
		}

		else if(((buffer_current_pos + measurement_length + HEADER_SIZE) < FLASH_DATA_BUFFER_SIZE) &&
				(is_measurement_container_empty(&measurement_container)))
		{
			//There is room in the current buffer for the full measurement.
			if(buffer_selection == BUFFER_A){
				memcpy(&data_bufferA[buffer_current_pos], &(measurement_container.data), measurement_length + HEADER_SIZE);
			}else if(buffer_selection == BUFFER_B){
				memcpy(&data_bufferB[buffer_current_pos], &(measurement_container.data), measurement_length + HEADER_SIZE);
			}
			
			buffer_current_pos += (measurement_length + HEADER_SIZE);
			
			//Reset the measurement.
			clear_buffer(measurement_container.data, sizeof(measurement_container_t));
			
		}else if(is_measurement_container_empty(&measurement_container))
		{
			
			//Split measurement across the buffers, and write to flash.
			uint8_t bytesInPrevBuffer = FLASH_DATA_BUFFER_SIZE - buffer_current_pos;
			uint8_t bytesLeft = (measurement_length + HEADER_SIZE) - bytesInPrevBuffer;
			
			//Put as much data as will fit into the almost full buffer.
			
			switch(buffer_selection)
			{
				case BUFFER_A:
				{
					memcpy(&data_bufferA[buffer_current_pos], &(measurement_container.data), bytesInPrevBuffer);
					buffer_selection = BUFFER_B;
					buffer_current_pos = 0;
					break;
				}
				case BUFFER_B:
				{
					memcpy(&data_bufferB[buffer_current_pos], &(measurement_container.data), bytesInPrevBuffer);
					buffer_selection = BUFFER_A;
					buffer_current_pos = 0;
					break;
				}
			}
			
			
			//Put the rest of the measurement in the next buffer.
			switch(buffer_selection)
			{
				case BUFFER_A:
				{
					memcpy(&data_bufferA[buffer_current_pos], &(measurement_container.data[bytesInPrevBuffer]), bytesLeft);
					buffer_current_pos = bytesLeft;
					break;
				}
				case BUFFER_B:
				{
					memcpy(&data_bufferB[buffer_current_pos], &(measurement_container.data[bytesInPrevBuffer]), bytesLeft);
					buffer_current_pos = bytesLeft;
					break;
				}
			}
			
			//reset the measurement.
			clear_buffer(measurement_container.data, sizeof(measurement_container_t));
			measurement_length = 0;
			
			//Flash write buffer not in use! then clear old buffer?
			switch(buffer_selection)
			{
				case BUFFER_A:
				{
					//We just switched to A so transmit B.
					if(IS_RECORDING(configParams->values.flags))
					{
						FlashStatus stat_f = flash_program_page(flash_ptr, flash_address, data_bufferB,
																   FLASH_DATA_BUFFER_SIZE);
						while(FLASH_IS_DEVICE_BUSY(stat_f))
						{
							stat_f = flash_get_status_register(flash_ptr);
							vTaskDelay(1);
						}
						
						flash_address += FLASH_DATA_BUFFER_SIZE;
						if(flash_address >= FLASH_SIZE_BYTES)
						{
							while(1)
							{ ;}
						}
						
					}else
					{
						transmit_bytes(huart, data_bufferB, 256);
					}
					break;
				}
				case BUFFER_B:
				{
					//We just switched to B so transmit A
					if(IS_RECORDING(configParams->values.flags))
					{
						FlashStatus stat_f2 = flash_program_page(flash_ptr, flash_address, data_bufferA,
																	FLASH_DATA_BUFFER_SIZE);
						while(FLASH_IS_DEVICE_BUSY(stat_f2))
						{
							stat_f2 = flash_get_status_register(flash_ptr);
							vTaskDelay(1);
						}
						
						flash_address += FLASH_DATA_BUFFER_SIZE;
						
						if(flash_address >= FLASH_SIZE_BYTES)
						{
							while(1)
							{ ; }
						}
					}else
					{
						transmit_bytes(huart, data_bufferA, 256);
					}
					break;
				}
			}
		}
		
		clear_buffer(measurement_container.data, sizeof(measurement_container_t));
		measurement_length = 0;
		
		if(!running){
			vTaskSuspend(NULL);
		}
	};
	
} // logging_task()


void _flash_write()
{
	int p;
	uint16_t buff_end = (ring_buff_size);
	for(p = 0; p < 25; p++)
	{
		//need to copy last portion into the bufferA. Make sure to start from right place, which wont be the next spot.
		if((buffer_index_curr + 256) < buff_end)
		{
			FlashStatus stat_f2 = flash_program_page(flash_ptr, flash_address, &launchpadBuffer[buffer_index_curr],
														FLASH_DATA_BUFFER_SIZE);
			while(FLASH_IS_DEVICE_BUSY(stat_f2))
			{
				stat_f2 = flash_get_status_register(flash_ptr);
				vTaskDelay(1);
			}
			buffer_index_curr += 256;
		}else
		{
			
			uint8_t buff_temp[256];
			memcpy(&buff_temp, &launchpadBuffer[buffer_index_curr], buff_end - buffer_index_curr);
			memcpy(&buff_temp[buff_end - buffer_index_curr], &launchpadBuffer, FLASH_DATA_BUFFER_SIZE - (buff_end - buffer_index_curr));
			
			FlashStatus stat_f2 = flash_program_page(flash_ptr, flash_address, buff_temp, FLASH_DATA_BUFFER_SIZE);
			while(FLASH_IS_DEVICE_BUSY(stat_f2))
			{
				stat_f2 = flash_get_status_register(flash_ptr);
				vTaskDelay(1);
			}
			buffer_index_curr = FLASH_DATA_BUFFER_SIZE - (buff_end - buffer_index_curr);
		}
		
		flash_address += FLASH_DATA_BUFFER_SIZE;
	}
	buffer_index_curr = 0;
}

void _landing()
{
	if(alt_count > 0)
	{
		
		//If altitude is within a 1m range for 20 samples
		if(altitude.float_val > (alt_prev.float_val - 1.0) && altitude.float_val < (alt_prev.float_val + 1.0))
		{
			alt_count++;
			if(alt_count > 245)
			{
				alt_count = 201;
			}
		}else
		{
			alt_count = 0;
		}
		
	}else
	{
		
		alt_prev.float_val = altitude.float_val;
		alt_count++;
		if(alt_count > 245)
		{
			alt_count = 201;
		}
	}
	
	if((pow(imu_reading.data_gyro.x, 2) + pow(imu_reading.data_gyro.y, 2) + pow(imu_reading.data_gyro.z, 2)) <
	   63075)
	{
		//If the gyro readings are all less than ~4.4 deg/sec and the altitude is not changing then the rocket has probably landed.
		
		if(alt_count > 200)
		{
			
			configParams->values.state = STATE_LANDED;
			configParams->values.flags = configParams->values.flags & ~(0x01);
			write_config(configParams);
			uint32_t header = (measurement.data[0] << 16) + (measurement.data[1] << 8) + measurement.data[2];
			header |= LAND_DETECT;
			
			measurement.data[0] = (header >> 16) & 0xFF;
			measurement.data[1] = (header >> 8) & 0xFF;
			measurement.data[2] = (header) & 0xFF;
			
			//Put everything into low power mode.
			running = 0;
			
		}
	}
}

void _launch()
{
	buzz(250);
	vTaskResume(*timerTask_h); //start fixed timers.
	configParams->values.state = STATE_IN_FLIGHT_PRE_APOGEE;
	configParams->values.flags = configParams->values.flags | 0x04;

	//Record the launch event.
	uint32_t header = (measurement.data[0] << 16) + (measurement.data[1] << 8) + measurement.data[2];
	header |= LAUNCH_DETECT;
	configParams->values.flags = configParams->values.flags | 0x01;
	write_config(configParams);
	
	measurement.data[0] = (header >> 16) & 0xFF;
	measurement.data[1] = (header >> 8) & 0xFF;
	measurement.data[2] = (header) & 0xFF;
	
	_flash_write();
}


void _parachute_deployment()
{
	if(altFiltered < 375.0)
	{
		//375m ==  1230 ft
		alt_main_count++;
	}else
	{
		alt_main_count = 0;
	}
	if(alt_main_count > 5)
	{
		//deploy main
		buzz(250);
		recoverySelect_t event = MAIN;
		enable_mosfet(event);
		activate_mosfet(event);
		continuityStatus_t cont = check_continuity(event);
		uint32_t header = (measurement.data[0] << 16) + (measurement.data[1] << 8) + measurement.data[2];
		header |= MAIN_DETECT;
		
		measurement.data[0] = (header >> 16) & 0xFF;
		measurement.data[1] = (header >> 8) & 0xFF;
		measurement.data[2] = (header) & 0xFF;
		
		if(cont == OPEN_CIRCUIT)
		{
			
			configParams->values.state = STATE_IN_FLIGHT_POST_MAIN;
			configParams->values.flags = configParams->values.flags | 0x10;
			write_config(configParams);
			uint32_t header = (measurement.data[0] << 16) + (measurement.data[1] << 8) + measurement.data[2];
			header |= MAIN_DEPLOY;
			measurement.data[0] = (header >> 16) & 0xFF;
			measurement.data[1] = (header >> 8) & 0xFF;
			measurement.data[2] = (header) & 0xFF;
			
		}
	}
}


void ascent()
{
	
	apogee_holdout_count++;
	if(apogee_holdout_count > (20 * 15))
	{
		
		uint64_t acc_mag =
			pow(imu_reading.data_acc.x, 2) + pow(imu_reading.data_acc.y, 2) + pow(imu_reading.data_acc.z, 2);
		if(acc_mag < 1 && altFiltered > 9000.0)
		{
			//5565132 = 3 * 1362^2 (aprox 0.5 g on all direction)
			//2438m -> 8,000 ft
			
			buzz(250);
			recoverySelect_t event = DROGUE;
			enable_mosfet(event);
			activate_mosfet(event);
			continuityStatus_t cont = check_continuity(event);
			uint32_t header = (measurement.data[0] << 16) + (measurement.data[1] << 8) + measurement.data[2];
			header |= DROGUE_DETECT;
			measurement.data[0] = (header >> 16) & 0xFF;
			measurement.data[1] = (header >> 8) & 0xFF;
			measurement.data[2] = (header) & 0xFF;
			
			if(cont == OPEN_CIRCUIT)
			{
				configParams->values.state = STATE_IN_FLIGHT_POST_APOGEE;
				configParams->values.flags = configParams->values.flags | 0x08;
				write_config(configParams);
				
				uint32_t header =
					(measurement.data[0] << 16) + (measurement.data[1] << 8) + measurement.data[2];
				header |= DROGUE_DEPLOY;
				measurement.data[0] = (header >> 16) & 0xFF;
				measurement.data[1] = (header >> 8) & 0xFF;
				measurement.data[2] = (header) & 0xFF;
				
			}
			
		}
	}
}

void _write_imu_data(imu_data_struct imu_reading, uint8_t* dest)
{
	uint16_t delta_t = imu_reading.time_ticks - prev_time_ticks;
	// Make sure time doesn't overwrite type and event bits.
	uint32_t header = (ACC_TYPE | GYRO_TYPE) + (delta_t & 0x0FFF);
	measurement_length = ACC_DATA_SIZE + GYRO_DATA_SIZE;
	prev_time_ticks = imu_reading.time_ticks;
	
	write_24(header,                  &dest[0]);
	write_16(imu_reading.data_acc.x,  &dest[3]);
	write_16(imu_reading.data_acc.y,  &dest[5]);
	write_16(imu_reading.data_acc.z,  &dest[7]);
	write_16(imu_reading.data_gyro.x, &dest[9]);
	write_16(imu_reading.data_gyro.y, &dest[11]);
	write_16(imu_reading.data_gyro.z, &dest[13]);
}


void _write_pressure_sensor_data(bmp_data_struct bmp_reading, uint8_t * dest)
{
	//We already have a imu reading.
	measurement_length += (PRES_DATA_SIZE + TEMP_DATA_SIZE + ALT_DATA_SIZE);
	
	//Update the header bytes.
	uint32_t header = (dest[0] << 16) + (dest[1] << 8) + dest[2];
	header |= PRES_TYPE | TEMP_TYPE;
	altitude = altitude_approx((float) bmp_reading.data.pressure, (float) bmp_reading.data.temperature, configParams);
	
	write_24(header, &dest[0]);
	write_24(bmp_reading.data.pressure,    &dest[15]);
	write_24(bmp_reading.data.temperature, &dest[18]);
	write_32(altitude.byte_val,            &dest[21]);
	
	altFiltered = altFiltered + (altitude.float_val - altFiltered) * 0.2;
}
